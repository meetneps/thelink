from django.urls import path
# from ytube import views
from . import views

from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('ytube/', views.listvideos),
    path('ytube/watch/<int:id>/', views.watchvideo, name='watch'),
    path('ytube/search/', views.searchvideo, name='search_results')
]