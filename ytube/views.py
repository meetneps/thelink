from django.shortcuts import render
from django.core.paginator import Paginator
from django.db.models import Q

from django.template import loader

from django.http import HttpResponse
from ytube.models import Video

def index(request):
    return HttpResponse("Hello, world. You're at the yt index.")


def listvideos(request):
    latest_video_list = Video.objects.all().order_by('-pubdate')
    # latest_video_list = Videos.objects.order_by('-pubdate')[:3]

    # Paginator
    paginator = Paginator(latest_video_list, 10) # Show 10 items

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'ytube/index.html', {'page_obj': page_obj})



def watchvideo(request, id):
    try:
        video = Video.objects.get(pk=id)
    except Video.DoesNotExist:
        raise Http404("Video does not exist")
    return render(request, 'ytube/detail.html', {'video': video})


# search
def searchvideo(request):

    if request.GET :
        print('Request GET')
        searched =  request.GET['searched']
        #video_list = Video.objects.filter(title__icontains=searched) # new
        video_list = Video.objects.filter(
                        Q(title__icontains=searched) | Q(description__icontains=searched))
        # Paginator
        paginator = Paginator(video_list, 10) # Show 10 items
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)

    return render(request, 'ytube/search.html',
                  {'page_obj': page_obj, 'searched': searched})