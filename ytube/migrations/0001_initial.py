# Generated by Django 3.2.2 on 2021-05-07 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('vid', models.CharField(blank=True, default='', max_length=255)),
                ('title', models.CharField(blank=True, default='', max_length=255)),
                ('description', models.CharField(blank=True, default='', max_length=500)),
                ('thumbnail', models.CharField(blank=True, default='', max_length=255)),
                ('pubdate', models.DateTimeField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AddIndex(
            model_name='video',
            index=models.Index(fields=['title'], name='ytube_video_title_89fad2_idx'),
        ),
        migrations.AddIndex(
            model_name='video',
            index=models.Index(fields=['description'], name='ytube_video_descrip_c52010_idx'),
        ),
        migrations.AddIndex(
            model_name='video',
            index=models.Index(fields=['pubdate'], name='ytube_video_pubdate_d93e3d_idx'),
        ),
    ]
