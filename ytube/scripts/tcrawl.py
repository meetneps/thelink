#!/usr/bin/python

# This sample executes a search request for the specified search term.
# Sample usage:
#   python search.py --q=surfing --max-results=10
# NOTE: To use the sample, you must provide a developer key obtained
#       in the Google APIs Console. Search for "REPLACE_ME" in this code
#       to find the correct place to provide that key..

import argparse
import json
from datetime import datetime
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = 'AIzaSyAkioYy-yXZgYSc1mylfWvNhNE__PaNF9w'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

def youtube_search(options):
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

  # Call the search.list method to retrieve results matching the specified
  # query term.
  search_response = youtube.search().list(
    q='kajal agarwal', # options.q,
    part='id,snippet',
    publishedAfter='2021-01-01T00:00:00Z',
    maxResults= 25, # options.max_results
  ).execute()

  videos = []
  channels = []
  playlists = []

# The publishedAfter parameter indicates that the API response
# should only contain resources created at or after the specified time.
# The value is an RFC 3339 formatted date-time value (1970-01-01T00:00:00Z).

  print( json.dumps( search_response , indent=4 ) )
  # Add each result to the appropriate list, and then display the lists of
  # matching videos, channels, and playlists.
  # for search_result in search_response.get('items', []):
  #   if search_result['id']['kind'] == 'youtube#video':
  #     videos.append('%s (%s)' % (search_result['snippet']['title'],
  #                                search_result['id']['videoId']))
  #   elif search_result['id']['kind'] == 'youtube#channel':
  #     channels.append('%s (%s)' % (search_result['snippet']['title'],
  #                                  search_result['id']['channelId']))
  #   elif search_result['id']['kind'] == 'youtube#playlist':
  #     playlists.append('%s (%s)' % (search_result['snippet']['title'],
  #                                   search_result['id']['playlistId']))
  #
  # print('Videos:\n', '\n'.join(videos), '\n')
  # print('Channels:\n', '\n'.join(channels), '\n')
  # print('Playlists:\n', '\n'.join(playlists), '\n')



if __name__ == '__main__':
    # Crawl youtube
    query='suki sivam'
    ytcrawl(query)

    # Save to db

    #videos = Video.objects.all()