#!/usr/bin/python

# This sample executes a search request for the specified search term.
# Sample usage:
#   python search.py --q=surfing --max-results=10
# NOTE: To use the sample, you must provide a developer key obtained
#       in the Google APIs Console. Search for "REPLACE_ME" in this code
#       to find the correct place to provide that key..

# @todo
# explore https://git.ahfc.us/energy/bmon/-/blob/bf5305db590b0000cfdba366562ad9f60751612b/bmsapp/scripts/run_periodic_scripts.py


import requests
import json
from datetime import datetime
import time
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from ytube.models import Video

# To cycle/rotate the API Keys
from itertools import cycle

from googleapiclient.errors import HttpError


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.

# Multiple API Keys, so that if one fails, other will be used.
# DEVELOPER_KEY_LIST = [
#                        'AIzaSyAkioYy-yXZgYSc1mylfWvNhNE__PaNF9w' ,
#                        'AIzaSyCv-NrpJSgsGk6k8TGJJwlup8U4IXQAZA0' ,
#                        'AIzaSyAx846VylltD2eh_VRhVLQ_ZAdYdYpLHss' ,
#                       ]


DEVELOPER_KEY_LIST = [
                       'AIzaSyATyX1EakBuzrhPpVdIccqBVQ1tUZXBfK4' ,
                       'AIzaSyAGsJTZtdJW0sbyJnGa2-xca6dlwTh8o78' ,
                       'AIzaSyCpaxquaS7VMknZOs3EKHd7OC1BXPmedpg' ,
                      ]

current_developer_key = 0

YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

# Global variable to track, retrieve next pages
nextpage_token = ''

# Custom encoder for json , used for datetime
def default(o):
    if isinstance(o, (datetime, datetime)):
        return o.isoformat()


def uni_decode(unicode_or_str):

  if isinstance(unicode_or_str, str):
    # print('Its pure string', unicode_or_str)
    text = unicode_or_str
    decoded = False
  else:
    # print('!!!!! Unicode', unicode_or_str)
    text = unicode_or_str.decode("utf-8")
    # print('!!! After decoding ', text )
    decoded = True

  return text


def save_searches(search_result):

  video_result = []
  for search_result in search_response.get('items', []):

        # get only youtube videos, not channels, not playlist
        if search_result['id']['kind'] == 'youtube#video':
          video_item = {}
          video_item["vid"] = search_result['id']['videoId']
          video_item["title"] =  uni_decode(search_result['snippet']['description']) # uni_decode(search_result['snippet']['title'])
          video_item["description"] = uni_decode(search_result['snippet']['description'])
          video_item["thumbnail"] = search_result['snippet']['thumbnails']['medium']['url']
          video_item["pub_date"] = search_result['snippet']['publishedAt']
          video_item["pub_datetime"] = datetime.strptime(video_item["pub_date"], "%Y-%m-%dT%H:%M:%S%z")

          video_result.append(video_item)

          # Save to db
          b = Video(vid=video_item["vid"],
                   title=video_item["title"],
                   description=video_item["description"],
                   thumbnail=video_item["thumbnail"],
                   pubdate=video_item["pub_datetime"]
                   )
          b.save()

          print('Next Page token', nextpage_token)
          print('Saving..' , video_item['title'] )


def youtube_search(q,pub_date):
  # Saving the next page token in global
  # To get the next developer key, if quota error
  global nextpage_token, current_developer_key
  while(1):

    try:
      # Call the search.list method to retrieve results matching the specified
      # query term.
      # Crawling YT API for videos
      # Pass the Next page token saved as page token to get the next page result
      # during the next run

      # To cycle throug the list of api keys available
      current_developer_key = current_developer_key % len(DEVELOPER_KEY_LIST)
      developerKey = DEVELOPER_KEY_LIST[ current_developer_key ]

      print('Dev Key Used [{0}]: {1} '.format(current_developer_key, developerKey ) )
      print('Query: [{0}]: Pub Date {1} '.format(q, pub_date ) )

      youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
      developerKey=developerKey)
      search_response = youtube.search().list(
        q=q, # options.q,
        part='id,snippet',
        publishedAfter= '2021-01-01T00:00:00Z', # pub_date,# '2021-01-01T00:00:00Z',
        maxResults= 25, # options.max_results
        pageToken=nextpage_token,
      ).execute()

      nextpage_token = search_response.get("nextPageToken")
      print('Next Page token', nextpage_token)
      save_searches(search_result)

    except HttpError as e:
        if e.resp.status in [403, 500, 503]:
          time.sleep(5)
          print('Exception happened')
          print (e)
          # print (search_response)
          current_developer_key += 1
          print('Exception happened ending....')
          continue
        else:
          raise

def run():

  try:
    now = datetime.now().utcnow()
    now = now.isoformat('T')+'Z'

    # now = datetime.utcnow()
    # d_with_timezone = d.replace(tzinfo=pytz.UTC)
    # d_with_timezone.isoformat()

    i = 0
    while(1):
      print('Run ---->', i)
      youtube_search(q='Beautiful women', pub_date=now, )
      time.sleep(10)
      i += 1

  except requests.HTTPError as e:
    print ('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))