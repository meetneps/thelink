from django.db import models

# Create your models here.
class Video(models.Model):
	# Autoincrement id
	id = models.BigAutoField(primary_key=True)
	vid = models.CharField(max_length=255,blank=True, default='')
	title = models.CharField(max_length=255, blank=True, default='')

	# I am using a long char field for description,
	description = models.CharField(max_length=500, blank=True, default='')
	thumbnail = models.CharField(max_length=255, blank=True, default='')
	pubdate = models.DateTimeField()
	created_on = models.DateTimeField(auto_now_add=True)

	# Creating required index
	# Since we search by title, desc, pubdate,
	# Creating three indices
	class Meta:
		   indexes = [
				models.Index(fields=['title',]),
				models.Index(fields=['description',]),
				models.Index(fields=['pubdate',]),
		  ]