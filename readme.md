# Readme 

# Non - docekerised approach

Run in the order
```
cd thelink
source thelink-venv/bin/activate
```

Requirements `pip install -r requirements.txt `

Migration: `python manage.py migrate`

Crawler:  `python manage.py runscript crawl`

Web app:  `python manage.py `runserver


# docekerised approach
```
docker-compose up
```
